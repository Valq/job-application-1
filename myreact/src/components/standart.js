import React from 'react';
import '../css/standart.css';
import view from '../images/view.png';

function Standart(){
	return(
		<standart> 
			<div className="flex">
				<div className="div"><h1 className="h1S">Standard Picture Section</h1>
				<hr className="lineStandart"/>
				<p className="pStandart"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod 
									    <br/>bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra 
										<br/>justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque 
										<br/>penatibus et magnis dis parturient montes.
										<br/>
										<br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod 
										<br/>bibendum laoreet.</p></div>
				<div><img src={view} className="view" alt="view" /></div>
			</div>
		</standart>
	)
}

export default Standart