import React from 'react';
import '../css/pricing.css';



function Pricing(){
	return(
		<pricing> 		
				<div className ="flexC">
					<div className="divBasic">
						<div className="nameB">Basic</div>
						<div className="priceB">$0</div>
						<div className="freeB">Free for Life</div>
						<div className="emptyB"></div>
						<div className="propertyGBOf">1 GB OF SPACE</div>
						<div className="propertyGB">10 GB OF BANDWIDTH</div>
						<div className="propertyWeb">3 WEBSITES</div>
						<div className="propertyBasic">BASIC CUSTOMIZATION</div>
						<div className="propertyWordpress">WORDPRESS INTEGRATION</div>
						<div className="propertyEmail">EMAIL SUPPORT</div>
					</div>
				
				<div >
				<h1 className="pricing"> PRICING OPTIONS </h1>
				<hr className="linePricing"/>
				<p className="pricingLorem"> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		
				
					<div className="professional">Professional</div>
					<div className="profPrice">$19</div>
					<div className="payment">Monthly Payment</div>
					<div className="mostPopular">OUR MOST POPULAR</div>
					<div className="propertyGBOfProf">5 GB OF SPACE</div>
					<div className="propertyGBProf">50 GB OF BANDWIDTH</div>
					<div className="propertyWebProf">12 WEBSITES</div>
					<div className="propertyBasicProf">ADVANCED CUSTOMIZATION</div>
					<div className="propertyWordpressProf">WORDPRESS INTEGRATION</div>
					<div className="propertyEmailProf">EMAIL SUPPORT</div>
				</div>
				
				<div>
					<div className="enterprise">Enterprise</div>
					<div className="priceE">$70 </div>
					<div className="paymentE">Monthly Payment</div>
					<div className="emptyE"></div>
					<div className="propertyGBOfEnter">UNLIMITED SPACE</div>
					<div className="propertyGBEnter">UNLIMITED BANDWIDTH</div>
					<div className="propertyWebEnter">100 WEBSITES</div>
					<div className="propertyBasicEnter">ADVANCED CUSTOMIZATION</div>
					<div className="propertyWordpressEnter">WORDPRESS INTEGRATION</div>
					<div className="propertyEmailEnter">24/7 CUSTOMER SUPPORT</div>
				</div>
			</div>
			
		</pricing>
	)
}

export default Pricing