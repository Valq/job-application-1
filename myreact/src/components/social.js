import React from 'react';
import '../css/social.css'
import media1 from '../images/media1.png';
import media2 from '../images/media2.png';
import media3 from '../images/media3.png';
import media4 from '../images/media4.png';
import media5 from '../images/media5.png';
import media6 from '../images/media6.png';
import media7 from '../images/media7.png';


function Social(){
	return(
		<social> 
			 <div className="flex-container">
			 	<div><h1 className="socialMedia"> Social Media </h1>
				<p className="lorem"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
				<br/>Aenean euismod bibendum laoreet.</p></div>
				
				<div><img src={media1}  className="media1" alt="media1" />
				<img src={media2} className="media2" alt="media2" />
				<img src={media3} className="media3" alt="media3" />
				<img src={media4} className="media4" alt="media4" />
				<img src={media5} className="media5" alt="media5" />
				<img src={media6} className="media6" alt="media6" />
				<img src={media7} className="media7" alt="media7" /></div>
			
			</div>
			<hr className="lines"/>
		</social>
	)
}

export default Social