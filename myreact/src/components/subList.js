import React from 'react';
import '../css/subList.css';
import sity from '../images/sity.png';
import cloudDown from '../images/cloudDown.png';
import cloudUp from '../images/cloudUp.png';

function SubList(){
	return(
		<subList> 
		
			<div className="flexContainerSub">
			<div>
				<img src={sity} className="sity" alt="sity" /></div>
				<div><h1 className="loremIp">Sub list section </h1>
				<hr className="lineSub"/>
				<p className="pLorem"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean 
					<br/>euismod bibendum laoreet. Proin gravida dolor sit amet lacus 
					<br/>accumsan et viverra justo commodo. </p>
					
					<div className="cloudUp"><img src={cloudUp}  alt="cloudUp" /></div>
				<div className="LI" ><h1 className="titleL">Title </h1>
				
				<p className="pLI">Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
					<br/>Aenean euismod bibendum laoreet. </p></div>
					
				<div><img src={cloudDown} className="cloudDown" alt="cloudDown" /></div>
				<div className="LIDown"><h1 className="titleLI">Title </h1>
				<p className="pLI"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
					<br/>Aenean euismod bibendum laoreet. Proin gravida dolor 
					<br/>sit amet lacus accumsan et viverra justo commodo. </p>
			</div>
			</div>
			
			</div>
		</subList>
	)
}

export default SubList