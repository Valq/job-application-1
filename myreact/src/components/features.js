import React from 'react';
import '../css/features.css';
import building from '../images/building.png';

function Features(){
	return(
		<features> 
			<div className="flContainer">
				<div className="">
					 <button className="active btn:hover">TAB1</button>
					 <button className="tabButton">TAB2</button>
					 <button className="tabButton">TAB3</button></div>
					 
					 <div><h1 className="tabTop"> Tabs with soft transitioning effect. </h1>
					<p className="p"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod 
									    <br/>bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra 
										<br/>justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque 
										<br/>penatibus et magnis dis parturient montes.
										<br/>
										<br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod 
										<br/>bibendum laoreet.</p>
					 
					  <button className="downloadbtn">Download</button>
					  </div>
					  
				<div><img src={building} className="building" alt="building" />
			</div>
			</div>
			
		</features>
	)
}

export default Features