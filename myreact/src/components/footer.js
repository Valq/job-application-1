import React from 'react';
import '../css/footer.css';
import media1 from '../images/media1.png';
import media2 from '../images/media2.png';
import media3 from '../images/media3.png';
import media4 from '../images/media4.png';
import media5 from '../images/media5.png';
import media6 from '../images/media6.png';
import media7 from '../images/media7.png';



function Footer(){
	return(
		<footer> 	
			<div className="footer">
				<div className="media">
				<img src={media1}  className="face" alt="media1" />
				<img src={media2} className="twit" alt="media2" />
				<img src={media3} className="gmail" alt="media3" />
				<img src={media4} className="print" alt="media4" />
				<img src={media5} className="insta" alt="media5" />
				<img src={media6} className="stumbleupon" alt="media6" />
				<img src={media7} className="rss" alt="media7" />
				   <h6 className="rights">© 2015 Axure Themes</h6>
			    </div>
			</div>
		</footer>
	)
}

export default Footer