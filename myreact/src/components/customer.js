import React from 'react';
import '../css/customer.css';
import avatar from '../images/avatar.png';
import avatar2 from '../images/avatar2.png';
import avatar3 from '../images/avatar3.png';
import talk from '../images/talk.png';
import talk2 from '../images/talk2.png';
import talk3 from '../images/talk3.png';



function Customer(){
	return(
		<customer> 	
<div className="flexCustomer">		
			<div>
				<img src={talk} className="talk" alt="talk" />
				
				<img src={avatar} className="avatar" alt="avatar" />
				<h4 className="h4">Jeremy H</h4>
				<h5 className="h5">Manager</h5>
				
				<p className="pCustomer">Lorem ipsum dolor sit amet, 
					<br/>consectetur adipisicing elit. Doloribus 
					<br/>accusamus expedita repellat similique 
					<br/>odio aspernatur ex, architecto eaque 
					<br/>quo suscipit.</p>
			</div>
			
			<div>
			<h1 className="customersSay">WHAT OUR CUSTOMERS ARE SAYING</h1>
				<hr className="linePricing"/>
				<p className="pricingLorem"> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				<img src={talk2} className="talk2" alt="talk2" />
				
				
				<h4 className="john">John S.</h4>
				<h5 className="freelancer">Freelancer</h5>
				<img src={avatar2} className="avatar2" alt="avatar2" />
				<p className="loremCust">Lorem ipsum dolor sit amet, 
					<br/>consectetur adipisicing elit. Doloribus 
					<br/>accusamus expedita repellat similique 
					<br/>odio aspernatur ex, architecto eaque 
					<br/>quo suscipit.</p>
			</div>
			
			<div>
				
				
				<img src={talk3} className="talk3" alt="talk3" />
				<h4 className="susan">Susan W.</h4>
				<h5 className="photographer">Photographer</h5>
				<img src={avatar3} className="avatar3" alt="avatar3" />
				<p className="loremPhoto">Lorem ipsum dolor sit amet, 
<br/>consectetur adipisicing elit. Doloribus 
<br/>accusamus expedita repellat similique 
<br/>odio aspernatur ex, architecto eaque 
<br/>quo suscipit.</p>
			</div>
			
	</div>		
		</customer>
	)
}

export default Customer