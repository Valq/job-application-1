import React from 'react';
import '../css/process.css';
import lightning from '../images/lightning.png';
import bulb from '../images/bulb.png';
import keyboard from '../images/keyboard.png';

function Process(){
	return(
		<process> 
			<div className="container">
				<div>
				<img src={bulb} className="bulb" alt="bulb" />
				<h1 className="thoughtful">Thoughtful Design</h1>
				<p className="paragraphL"> Lorem ipsum dolor sit amet, consectetur adipiscing 
					<br/>elit. Aenean euismod bibendum laoreet. Proin gravida 
					<br/>dolor sit amet lacus accumsan et viverra.</p>
				</div>	
				
					<div><h1 className="whyThisIs">WHY THIS IS AWESOME</h1>
				<hr className="lineProcess"/>
				<p className="whyLorem"> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				
				<img src={keyboard} className="keyboard" alt="keyboard" />
				<h1 className="well">Well Crafted</h1>
				<p className="paragraphWell">Lorem ipsum dolor sit amet, consectetur adipiscing 
					<br/>elit. Aenean euismod bibendum laoreet. Proin gravida 
					<br/>dolor sit amet lacus accumsan et viverra. </p></div>
				
					
			<div>		
				<img src={lightning} className="lightning" alt="lightinng" />
				<h1 className="customize">Easy to Customize</h1>
				<p className="paragraphEasy">Lorem ipsum dolor sit amet, consectetur adipiscing 
					<br/>elit. Aenean euismod bibendum laoreet. Proin gravida 
					<br/>dolor sit amet lacus accumsan et viverra. </p>
				</div>	
					
			</div>
		</process>
	)
}

export default Process