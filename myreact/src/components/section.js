import React from 'react';
import '../css/section.css';
import back from '../images/back.jpg';

function Section(){
	return(
		<section> 
			<div className="back">
				<h1 className="STYLISH_AXURE_DESIGN">STYLISH AXURE DESIGN</h1>
				<hr className="lineWhite"/>
				<p className="Use_the_sections"> Use the sections you need remove the ones you don't. Create gorgeous prototypes faster than ever!</p>
				<div> <button className="download">Download</button></div>
				
			</div>
		</section>
	)
}

export default Section