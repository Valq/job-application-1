import React from 'react';
import '../css/navbar.css'
import Logo from '../images/Logo.png';


function Navbar(){
	return(
		 <navbar>
		<div  id="navbar">
		<ul className="navbar ">
			<img src={Logo} className="logo" alt="logo" />
			<li><a className="navbar-content" href="#features">Features</a></li>
				<li><a className="navbar-content" href="#about">About</a></li>
				<li><a className="navbar-content" href="#pricing">Pricing</a></li>
				<li><a className="navbar-content" href="#reviews">Reviews</a></li>
				<li><a className="navbar-content" href="#contact">Contact</a></li>
			</ul>
		</div>
		
		</navbar>
	)
}

export default Navbar
		
		