import React from 'react';

import Navbar from './components/navbar';
import Header from './components/header';
import Social from './components/social';
import Features from './components/features';
import SubList from './components/subList';
import Standart from './components/standart';
import Process from './components/process';
import Pricing from './components/pricing';
import Customer from './components/customer';
import Section from './components/section';
import Contact from './components/contact';
import Footer from './components/footer';


function App() {
  return (
	<div>
		<Navbar />
		<Header />
		<Social />
		<Features />
		<SubList />
		<Standart />
		<Process />
		<Pricing />
		<Customer />
		<Section />
		<Contact />
		<Footer />
	</div>	
  );
}

export default App;
